package com.example.dodi_1202160178_si4001_pab_modul1;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private EditText alas;
    private EditText tinggi;
    private TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void hitung(View view){
        alas = findViewById(R.id.editText);
        tinggi = findViewById(R.id.editText2);
        hasil = findViewById(R.id.textView3);

        Integer Aalas = Integer.parseInt(alas.getText().toString());
        Integer Ttinggi = Integer.parseInt(tinggi.getText().toString());
        Integer hasile = Aalas*Ttinggi;
        hasil.setText(String.valueOf(hasile));
    }

}
